import service from '/service/service.js'
const { dom, data } = service

class AttributeElement {
  element
  constructor(element) {
    this.element = element
  }

  setAttributes(attributes) {
    for (const key in attributes) {
      if (Object.hasOwnProperty.call(attributes, key)) {
        const value = attributes[key]
        this.element[key] = value
      }
    }
  }

  create() {
    return this.element
  }
}

class StrategyAttributeTool {
  strategy
  element
  constructor() {

  }

  setElement(element) {
    this.element = element
  }

  setStrategy(strategy) {
    this.strategy = strategy
  }

  getElement() {
    return this.strategy.create()
  }
}

new class App {
  current_dom

  constructor() {
    this.init()
  }

  init() {
    if (data.tag.length % 2) data.tag.push({ name: '' })
    const fragment = document.createDocumentFragment()
    data.tag.forEach((tag) => {
      const button = document.createElement('button')
      button.classList = 'button-element'
      button.textContent = tag.name
      if (!tag.name) button.classList.add('hidden')
      fragment.append(button)
    })
    dom.el.element_box.append(fragment)

    dom.el.element_box.addEventListener('click', this.clickElementBox.bind(this))
    dom.el.preview_box.addEventListener('click', this.clean.bind(this))
    dom.el.preview.addEventListener('click', this.clickPreview.bind(this))
    dom.el.download.addEventListener('click', this.clickDownload.bind(this))
  }

  clean() {
    this.current_dom = null
    dom.el.nav_title.innerText = ''
    dom.$$$('.preview-default', dom.el.preview_box).forEach(dom => {
      dom.classList.remove('preview-highlight')
    })
  }

  clickElementBox(event) {
    const target = event.target
    if (target.classList.contains('button-element')) {
      const element = document.createElement(target.textContent)
      element.classList = 'preview-default'
      element.title = target.textContent
      element.textContent = target.textContent
      if (target.textContent === 'input' || target.textContent === 'textarea') {
        element.readOnly = true
      }
      if (this.current_dom) this.current_dom.append(element)
      else dom.el.preview.append(element)
    }
  }

  clickPreview(event) {
    event.stopPropagation()
    this.clean()
    const target = event.target
    this.current_dom = target
    const tag_name = target.tagName.toLowerCase()
    dom.el.nav_title.innerText = target.id ? tag_name + '#' + target.id : tag_name
    if (target.classList.contains('preview-default')) {
      target.classList.add('preview-highlight')
      const text = target.title
      const tag = data.tag.find((tag) => text.startsWith(tag.name))
      if (!tag) return
      dom.el.attribute_box.innerHTML = ''
      dom.el.attribute_box.append(this.createAttribute(tag.attributes, target))
    }
  }

  // 创建dom工具
  createAttribute(attributes, target) {
    const fragment = document.createDocumentFragment()
    let element, p, input, is_class, input_name = 'input'
    attributes.forEach((attribute) => {
      // 享元
      element = document.createElement('section')
      element.classList = 'attribute-editor-box'
      is_class = attribute.name === 'class'
      input_name = is_class ? 'textarea' : 'input'
      p = document.createElement('p')
      p.textContent = attribute.name
      p.classList = 'attribute-editor-title'
      p.title = attribute.description
      input = document.createElement(input_name)
      input.classList = `attribute-editor-${input_name}`
      input.value = target.getAttribute(attribute.name) || ''
      // 0.3s延迟处理
      let timeout
      input.oninput = function (e) {
        if (timeout) clearTimeout(timeout)
        timeout = setTimeout(() => {
          target.setAttribute(attribute.name, this.value)
        }, 500)
      }
      element.append(p)
      element.append(input)
      fragment.append(element)
    })
    const button = document.createElement('button')
    button.textContent = '删除'
    button.classList = 'attribute-delete-button'
    button.onclick = function () {
      dom.el.attribute_box.innerHTML = ''
    }
    fragment.append(button)
    return fragment
  }

  clickDownload() {
    const a = document.createElement('a')
    const blob = new Blob([dom.el.preview.innerHTML], { type: 'text/plain' })
    a.href = URL.createObjectURL(blob)
    a.download = 'demo.html'
    a.click()
  }
}